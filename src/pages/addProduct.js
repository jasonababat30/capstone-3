import React,{useState,useEffect,useContext} from 'react'

// Bootstrap
import {Form, Button, Container} from 'react-bootstrap'

// Sweetalert
import Swal from 'sweetalert2'

// UserContext
import UserContext from '../userContext'

// Redirect
import {Redirect} from 'react-router-dom'

 export default function AddProduct(){

 	// States
 	const [name, setName] = useState("")
 	const [category, setCategory] = useState("")
 	const [description, setDescription] = useState("")
 	const [price, setPrice] = useState(null)
 	const [isActive, setIsActive] = useState(false)

 	// useContext()
 	const {user,setUser} = useContext(UserContext)

 	// Use Effect
 	useEffect(()=> {

 		if(name !== "" && description !== "" && category !== "" && price !== null){

 			setIsActive(true)

 		} else {

 			setIsActive(false)

 		}

 	},[name,description,category,price])

 	function addProduct(e){

 		e.preventDefault()

 		fetch('https://floating-taiga-76973.herokuapp.com/amntn/activeProducts', {

 			method: "POST",
 			headers: {

 				'Content-Type' : 'application/json',
 				'Authorization' : `Bearer ${localStorage.getItem('token')}`

 			},
 			body: JSON.stringify({

 				name: name,
 				category: category,
 				description: description,
 				price: price

 			})
 		}).then(response => response.json()).then(data => {

 			if(!data.addedData){

 				Swal.fire({

 					icon: "error",
 					title: "Adding a Product was unsuccessful!",
 					text: data.message

 				})

 			} else {

 				Swal.fire({

 					icon: "success",
 					title: "Success!",
 					text: `You have successfully added ${data.addedData.name}`

 				})	

 			}

 		})

 		setName("")
 		setCategory("")
 		setDescription("")
 		setPrice(null)

 	}

 	return(
 		!user.isAdmin
 		?
 		<Redirect to="/" />
 		:
 			<>
 				<Container id="addProduct-container">
 					<h1 id="addProduct-heading">Add a Product</h1>
 					<Form id="addProduct-form" onSubmit={e => addProduct(e)}>
 						<Form.Group>
 							<Form.Label className="addProduct-label">
 								Product Name:
 							</Form.Label>
 							<Form.Control type="text" placeholder="Enter Product Name" value={name} onChange={e => {setName(e.target.value)}} required/>
 						</Form.Group>
 						<Form.Group>
 							<Form.Label className="addProduct-label">
 								Product Category:
 							</Form.Label>
 							<Form.Check name="category" label="Handgun" type="radio" onClick={e => {setCategory("Handgun")}}/>
 							<Form.Check name="category" label="Sub-machine gun" type="radio" onClick={e => {setCategory("Sub-machine gun")}}/>
 							<Form.Check name="category" label="Rifle" type="radio" onClick={e => {setCategory("Rifle")}}/>
 						</Form.Group>
 						<Form.Group>
 							<Form.Label className="addProduct-label">
 								Product Description:
 							</Form.Label>
 							<Form.Control type="text" placeholder="Enter Product Description" value={description} onChange={e => {setDescription(e.target.value)}} required/>
 						</Form.Group>
 						<Form.Group>
 							<Form.Label className="addProduct-label">
 								Product Price:
 							</Form.Label>
 							<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}} placeholder="Enter Product Price" min="50" required/>
 						</Form.Group>
 						{
 							isActive
 							? 
 							<Button variant="danger" id="addProduct-button" size="lg" type="submit">Add Product</Button>
 							: 
 							<Button variant="danger" id="addProduct-button" size="lg" type="submit" disabled>Add Product</Button>
 						}
 					</Form>
 				</Container>
 			</>

 		)

 }