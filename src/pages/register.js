import React, {useState,useEffect,useContext} from 'react'

// Bootstrap Components
import {Container, Card, Form, Button} from 'react-bootstrap'

// UserContext
import UserContext from '../userContext'

// Sweetalert
import Swal from 'sweetalert2'

// Redirect
import {Redirect} from 'react-router-dom'


export default function Register(){

	// States
	const [firstName , setFirstName] = useState("")
	const [lastName , setLastName] = useState("")
	const [age , setAge] = useState(0)
	const [sex , setSex] = useState(null)
	const [address , setAddress] = useState("")
	const [contactNo , setContactNo] = useState(0)
	const [email , setEmail] = useState("")
	const [userName , setUserName] = useState("")
	const [password , setPassword] = useState("")
	const [confirmPassword , setConfirmPassword] = useState("")
	const [cash , setCash] = useState(0)

	// User Context
	const {user,setUser} = useContext(UserContext)

	// Conditional Rendering for button
	const [isActive,setIsActive] = useState(true)
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && age !== 0 && sex !== null && address !== "" && contactNo !== 0 && email !== "" && userName !== "" && password !== "" && confirmPassword !== "" && cash !== 0) && (password === confirmPassword) && (contactNo.length === 11)){

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	}, [firstName, lastName, age, sex, address, contactNo, email, userName, password, confirmPassword, cash])

	function registerUser(e){

		e.preventDefault()

		fetch('https://floating-taiga-76973.herokuapp.com/amntn/register',{

			method: 'POST',
			headers: {
				'Content-type': "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				age: age,
				sex: sex,
				address: address,
				contactNo: contactNo,
				email: email,
				userName: userName,
				password: password,
				cash: cash
			})
		}).then(response => response.json()).then(data => {

			if(!data.data){

				Swal.fire({

					icon:"error",
					title: "Registration Failed!",
					text: data.message
				})

			} else {

				localStorage.setItem('email', data.data.email)
				localStorage.setItem('isAdmin', data.data.isAdmin)
				localStorage.setItem('isGoldMember', data.data.isGoldMember)
				localStorage.setItem('couponStatus', data.data.coupon.couponStatus)
				localStorage.setItem('couponValue', data.data.coupon.couponValue)
				localStorage.setItem('sex', data.data.sex)
				localStorage.setItem('cash', data.data.cash)
				localStorage.setItem('fullName', data.data.fullName)
				localStorage.setItem('id', data.data._id)

				setUser({
					email: data.data.email,
					isAdmin: data.data.isAdmin,
					isGoldMember: data.data.isGoldMember,
					couponStatus: data.data.couponStatus,
					couponValue: data.data.couponValue,
					sex: data.data.sex,
					fullName: data.data.fullName,
					cash: data.data.cash,
					id: data.data._id
				})

				if(data.data.sex === "Male"){

					Swal.fire({
						icon: "success",
						title: "Registration Successful!",
						text: `Welcome to Ammunation, Mr. ${data.data.fullName}!`
					})

					setWillRedirect(true)

				} else {

					Swal.fire({
						icon: "success",
						title: "Registration Successful!",
						text: `Welcome to Ammunation, Mrs. ${data.data.fullName}!`
					})

					setWillRedirect(true)

				}
			}
		})

		setFirstName("")
		setLastName("")
		setAge(0)
		setSex(null)
		setAddress("")
		setContactNo(0)
		setEmail("")
		setUserName("")
		setPassword("")
		setCash(0)
	}

	return(
		user.email
		?
		<Redirect to="/" />
		:
			willRedirect
			?
			<Redirect to="/login" />
			:
		<Container id="register-container">
			<Card id="register-card">
				<h1 id="register-heading">Register</h1>
				<Form id="register-form" className="container-fluid" onSubmit={e => registerUser(e)}>
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form. Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => {setFirstName(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form. Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => {setLastName(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Age:</Form.Label>
						<Form. Control type="number" placeholder="Enter Your Age" min="20" max="100" onChange={e => {setAge(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Sex:</Form.Label>
						<Form.Check name="sex" label="Male" type="radio" onClick={e => {setSex("Male")}}/>
						<Form.Check name="sex" label="Female" type="radio" onClick={e => {setSex("Female")}}/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Address:</Form.Label>
						<Form. Control type="text" placeholder="Enter Your Address" onChange={e => {setAddress(e.target.value)}}required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Contact No:</Form.Label>
						<Form. Control type="number" placeholder="Enter Your Contact Number" onChange={e => {setContactNo(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form. Control type="email" placeholder="Enter Your Email Address" value={email} onChange={e => {setEmail(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Username:</Form.Label>
						<Form. Control type="text" placeholder="Enter Your Username" onChange={e => {setUserName(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form. Control type="password" placeholder="Enter Your Password" onChange={e => {setPassword(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm Password:</Form.Label>
						<Form. Control type="password" placeholder="Re-type Your Password Choice" onChange={e => {setConfirmPassword(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Cash:</Form.Label>
						<Form. Control type="number" placeholder="Enter Your Cash" min="100" onChange={e => {setCash(e.target.value)}} required/>
					</Form.Group>
					{
						isActive
						? <Button type="submit" className="register-button">Register</Button>
						: <Button type="submit" className="register-button" disabled>Register</Button>
					}
				</Form>
			</Card>
		</Container>

		)

}
