import React from 'react'

// Image Logo
import cowboy from '../images/cowboy.png'

// Bootstrap
import {Jumbotron, Button} from 'react-bootstrap'

// Link
import {Link} from 'react-router-dom'

export default function NotFound(){

	let page = window.location.href

	let currentPage = page.slice(21,page.length)

	function backToHomepage(){

		window.location.replace('/')

	}

	return(

			<Jumbotron id="notFound-banner">
				<div id="notFound-content">
					<img id="notFound-logo" alt="logo" src={cowboy} className="img-fluid"/>
					<h1 id="notFound-heading">Page NOT FOUND!</h1>
					<p id="notFound-paragraph">The request URL: "<span id="notFound-span">{currentPage}</span>" was not found on the sever.</p>
					<Button size="lg" variant="info" id="notFound-button" onClick={backToHomepage}>Back to Homepage</Button>
					
				</div>
			</Jumbotron>

		)

}