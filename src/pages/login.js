import React, {useState, useEffect, useContext} from 'react'

// Bootstrap Components
import {Container, Form, Button, Card, Row, Col} from 'react-bootstrap'

// Logo
import logo from '../images/cowboy.png'

// UserContext
import UserContext from '../userContext'

// SweetAlert2
import Swal from 'sweetalert2'

// Redirect
import {Redirect} from 'react-router-dom'

export default function Login (){

	// States
	const[userName, setUserName] = useState("")
	const[password,setPassword] = useState("")
	const[isActive,setIsActive] = useState(true)

	// UserContext
	const {user,setUser} = useContext(UserContext)

	// State for Redirect
	const [willRedirect,setWillRedirect] = useState(false)

	// useEffect
	useEffect(() => {

		if(userName !== "" && password !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[userName,password])

	// Login Function
	function loginUser(e){

		e.preventDefault()

		fetch('https://floating-taiga-76973.herokuapp.com/amntn/login', {

			method: 'POST',
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				userName: userName,
				password: password
			})
		}).then(response => response.json()).then(data => {

			if(!data.accessToken){

				Swal.fire({
					icon: "error",
					title: "Login Failed",
					text: data.message
				})

			} else {

				console.log(data)

				localStorage.setItem('token', data.accessToken)

				fetch('https://floating-taiga-76973.herokuapp.com/amntn/profile', {

					headers: {

						'Authorization': `Bearer ${data.accessToken}`
					},
				}).then(response => response.json()).then(data => {

					console.log(data)

					localStorage.setItem('email', data.profileDetails.email)
					localStorage.setItem('isAdmin', data.profileDetails.isAdmin)
					localStorage.setItem('isGoldMember', data.profileDetails.isGoldMember)
					localStorage.setItem('couponStatus', data.profileDetails.coupon.couponStatus)
					localStorage.setItem('couponValue', data.profileDetails.coupon.couponValue)
					localStorage.setItem('sex', data.profileDetails.sex)
					localStorage.setItem('cash', data.profileDetails.cash)
					localStorage.setItem('fullName', data.profileDetails.fullName)
					localStorage.setItem('id', data.profileDetails._id)

					setUser({
						email: data.profileDetails.email,
						isAdmin: data.profileDetails.isAdmin,
						isGoldMember: data.profileDetails.isGoldMember,
						couponStatus: data.profileDetails.couponStatus,
						couponValue: data.profileDetails.couponValue,
						sex: data.profileDetails.sex,
						fullName: data.profileDetails.fullName,
						cash: data.profileDetails.cash,
						id: data.profileDetails._id
					})

					setWillRedirect(true)

					if(data.profileDetails.sex === 'Male'){

						Swal.fire({
							icon: "success",
							title: "Login Successful!",
							text: `You have successfully logged in, Mr. ${data.profileDetails.fullName}`
						})
					} else {

						Swal.fire({
							icon: "success",
							title: "Login Successful!",
							text: `You have successfully logged in, Mrs. ${data.profileDetails.fullName}`
						})
					}

				})

			}

		})

		setUserName("")
		setPassword("")

	}

	return (

		user.email || willRedirect
		?
		<Redirect to="/" />
		:
		<Container id="login-container">
			<Card id="login-card">
				<h1 id="login-heading">Login</h1>
				<Card.Body>
					<Form id="login-form" onSubmit={e => loginUser(e)}>
						<Form.Group>
							<Form.Label>Username:</Form.Label>
							<Form.Control type="text" placeholder="Enter Username" onChange={e => {setUserName(e.target.value)}} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control type="password" placeholder="Enter Password" onChange={e => {setPassword(e.target.value)}} required/>
						</Form.Group>
						{
							isActive
							? <Button className="login-button" type="submit">Login</Button>
							: <Button className="login-button" type="submit" disabled>Login</Button>
						}
					</Form>
				</Card.Body>					
			</Card>	
		</Container>
		

		)

}

