import React, {useState,useEffect,useContext} from 'react'

// Bootstrap
import {Table, Button, Row, Col, Container, Card} from 'react-bootstrap'

// Components
import Product from '../components/Product'
import AdminDashboard from '../components/AdminDashboard'

// User Context
import UserContext from '../userContext'

export default function Products(){

	// States
	const [allProducts, setAllProducts] = useState([])
	const [activeProducts, setActiveProducts] = useState([])
	const [update, setUpdate] = useState(0)

	// Use Context
	const {user,setUser} = useContext(UserContext)

	useEffect(() => {

		fetch('https://floating-taiga-76973.herokuapp.com/amntn/products', {

			headers:{
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {

			console.log(data.data)

			// All Products
			setAllProducts(data.data)

			let productsTemp = data.data

			let tempArray = productsTemp.filter(product => {

				return product.isActive === true

			})

			// all Active Products
			setActiveProducts(tempArray)

		})



	},[update])

	console.log(activeProducts)

	let productRows = allProducts.map(product => {

		return(

				product

			)
	})

	let productComponents = activeProducts.map(product => {

		return(

			product

			)

	})

	return(

		user.isAdmin
		?
			<AdminDashboard />
		:
			<Product productProp={productComponents}/>

		)

}
