import React from 'react'

// Global User Context
const UserContext = React.createContext()

// Context Provider
export const UserProvider = UserContext.Provider

export default UserContext
