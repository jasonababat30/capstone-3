import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

// IMPORT react-router-dom
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

// Imported COMPONENTS:
import NavBar from './components/NavBar'
import Home from './pages/home'
import Footer from './components/Footer'
import Login from './pages/login'
import Register from './pages/register'
import Products from './pages/products'
import AddProduct from './pages/addProduct'
import OrderProduct from './components/OrderProduct'
import Purchases from './components/Purchases'
import NotFound from './pages/notFound'

// Import Context Provider
import {UserProvider} from './userContext'

function App(){

  // Global User Context
  const [user,setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true",
    isGoldMember: localStorage.getItem('isGoldMember') === "true",
    couponStatus: localStorage.getItem('couponStatus') === "true",
    couponValue: localStorage.getItem('couponValue'),
    sex: localStorage.getItem('sex'),
    fullName: localStorage.getItem('fullName'),
    cash: localStorage.getItem('cash'),
    id: localStorage.getItem('id')
  })

  // Clearing All Local Storage Data
  function unsetUser(){

    localStorage.clear()

  }

  console.log(user)

  return(
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <header fluid>
            <NavBar />
          </header>
          <main id="main" fluid>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/register" component={Register} />
              <Route exact path="/products" component={Products} />
              <Route exact path="/addProduct" component={AddProduct} />
              <Route exact path="/orderProduct" component={OrderProduct} />
              <Route exact path="/purchases" component={Purchases} />
              <Route component={NotFound} />
            </Switch>
          </main>
          <footer id="footer" fluid>
            <Footer />
          </footer>
        </Router>
      </UserProvider>
    </>

    )

}

export default App

