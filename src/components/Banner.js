import React from 'react'

// Bootstrap Components
import {Jumbotron, Button} from 'react-bootstrap'

export default function Banner(){

	return(

		<Jumbotron id="jumbotron" expand="lg">
		  <div id="jumbotron-content">
		  	<h1 id="jumbotron-heading">Ammunation</h1>
		  	<p id="jumbotron-paragraph">
		  	  A place where modern problems require modern solutions!
		  	</p>
		  </div>
		</Jumbotron>

		)

}