import React, {useState, useEffect, useContext} from 'react'

// UserContext
import UserContext from '../userContext'

// Bootstrap
import {Container, Table, Row, Col, Card} from 'react-bootstrap'

// Redirect
import {Redirect} from 'react-router-dom'

export default function Purchases(){

	// State/s
	const [orders, setOrders] = useState([])
	const [haveOrders, setHaveOrders] = useState(false)

	// UserContext
	const {user, setUser} = useContext(UserContext)

	// UseEffect
	useEffect(() => {

		fetch(`https://floating-taiga-76973.herokuapp.com/amntn/order/userOrders`, {

			headers: {

				'Authorization' : `Bearer ${localStorage.getItem('token')}`

			}

		}).then(response => response.json()).then(data => {

			if(!data.foundData){

				setHaveOrders(false)

			} else {

				setHaveOrders(true)

				console.log(data.foundData)

				let userOrders = data.foundData

				setOrders(userOrders)

			}

		})

	},[])

	console.log(orders)

	let orderComponents = orders.map(order => {

		return(

			<tr>
				<td>{order.product.name}</td>
				<td>{order.orderQuantity}</td>
				<td>${order.totalAmount}</td>
			</tr>

			)

	})


	return(

			!user.email || user.isAdmin
			?
			<Redirect to="/" />
			:
			<Container id="purchases-container">
				<Row id="purchases-row">
					<Col xs={12} md={6}>
						<h1>Your Purchases</h1>
					</Col>
					<Col xs={12} md={6}>
						<h1>Cash: ${user.cash}</h1>
					</Col>
				</Row>
				{
					haveOrders
					?
						<Table id="purchases-table">
							<thead>
								<tr>
									<th>Product</th>
									<th>Number of Orders</th>
									<th>Total Amount</th>
								</tr>
							</thead>
							<tbody>
								{orderComponents}
							</tbody>
						</Table>
					:
						<h1 id="purchases-noOrders">You haven't Made any orders yet!</h1>
				}
				
			</Container>

		)

}