import React,{useContext} from 'react'

// Logo
import logoPic from '../images/cowboy.png'

// Bootstrap Components:
import {Navbar, Container} from 'react-bootstrap'
import {Nav} from 'react-bootstrap'

// NavLinks and Links
import {NavLink, Link} from 'react-router-dom'

// Import UserContext
import UserContext from '../userContext'

export default function NavBar(){

	const {user,unsetUser,setUser} = useContext(UserContext)

	// Log out function
	function logout(){

		unsetUser()

		setUser({
			email: null,
			isAdmin: null,
			isGoldMember: null,
			couponStatus: null,
			couponValue: null,
			sex: null,
			fullName: null,
			cash: null
		})

		window.location.replace('/')

	}

	return(

		<Navbar className="navBar" expand="lg">
			<Navbar.Brand as={Link} to="/"><img src={logoPic} alt="agent with gun" className="logo"/></Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse>
			<Container className="justify-content-start">
				<Nav>
					{
						!user.isAdmin
						?
							!user.email
							?
								<>
									<Nav.Item className="homeProductsRegisterMenu">
										<Nav.Link as={NavLink} to="/"className="navLinks">Home</Nav.Link>
									</Nav.Item>
									<Nav.Item className="homeProductsRegisterMenu">
										<Nav.Link as={NavLink} to="/products" className="navLinks">Products</Nav.Link>
									</Nav.Item>
								</>
								:
								<>
									<Nav.Item className="homeProductsRegisterMenu">
										<Nav.Link as={NavLink} to="/"className="navLinks">Home</Nav.Link>
									</Nav.Item>
									<Nav.Item className="homeProductsRegisterMenu">
										<Nav.Link as={NavLink} to="/products" className="navLinks">Products</Nav.Link>
									</Nav.Item>
									<Nav.Item className="homeProductsRegisterMenu">
										<Nav.Link as={NavLink} to="/purchases" className="navLinks">Purchases</Nav.Link>
									</Nav.Item>
								</>
						:
						<>
							<Nav.Item className="homeProductsRegisterMenu">
								<Nav.Link as={NavLink} to="/"className="navLinks">Home</Nav.Link>
							</Nav.Item>
							<Nav.Item className="homeProductsRegisterMenu">
								<Nav.Link as={NavLink} to="/products" className="navLinks">Products</Nav.Link>
							</Nav.Item>
							<Nav.Item className="homeProductsRegisterMenu">
								<Nav.Link as={NavLink} to="/addProduct" className="navLinks">Add Product</Nav.Link>
							</Nav.Item>
						</>
					}
					
				</Nav>
			</Container>
			<Container className="justify-content-end">
				<Nav>
					{
						user.email
						?
								<Nav.Item>
									<Nav.Link id="logout-menu" onClick={logout}>Logout</Nav.Link>
								</Nav.Item>
						:
							<>
								<Nav.Item className="homeProductsRegisterMenu">
									<Nav.Link as={NavLink} to="/register" className="navLinks">Register</Nav.Link>
								</Nav.Item>
								<Nav.Item>
									<Nav.Link as={NavLink} to="/login" className="navLinks">Login</Nav.Link>
								</Nav.Item>
							</>
					}
				</Nav>
			</Container>
			</Navbar.Collapse>
		</Navbar>

		)

}
/*
	<Nav.Item className="homeProductsRegisterMenu">
		<Nav.Link className="navLinks">Register</Nav.Link>
	</Nav.Item>
	<Nav.Item>
		<Nav.Link className="navLinks">Login</Nav.Link>
	</Nav.Item>
*/