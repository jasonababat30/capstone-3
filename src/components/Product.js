import React, {useState,useEffect,useContext} from 'react'

import {Card, Row, Col, Container, Jumbotron} from 'react-bootstrap'

import handgun from '../images/handgun-category.png'
import rifle from '../images/rifle-category.png'
import smg from '../images/smg-category.png'

// Component
import OrderProduct from './OrderProduct'

// Redirect
import {Redirect} from 'react-router-dom'

// UserContext
import UserContext from '../userContext'


export default function Product({productProp}){

	// console.log(productProp)

	// States
	const [willRedirect, setWillRedirect] = useState(false)

	// UserContext
	const {user,setUser} = useContext(UserContext)

	function orderProduct(productId){

		setWillRedirect(true)

		localStorage.setItem('productId', productId)

	}

	let productsCard = productProp.map(product => {

		return (
			!user.email
			?
			<Col xs={12} md={4} className="productColumns">
				{
					product.category === 'Handgun'
					?
					<Card key={product._id} className="productCards">
						<Card.Body>
							<Card.Img className="product-image" src={handgun} />
							<Card.Title className="product-name">{product.name}</Card.Title>
							<Card.Subtitle className="product-category">{product.category}</Card.Subtitle>
							<Card.Text className="product-text">{product.description}</Card.Text>
							<Card.Text className="product-text">${product.price}</Card.Text>
							{
								product.sale.saleStatus
								?
								<>
									<Card.Text className="product-text onSale">On Sale</Card.Text>
									<Card.Text className="product-text onSale">{product.sale.saleValue * 100}%</Card.Text>
								</>
								:
								null
							}
						</Card.Body>
					</Card>	
					:
						product.category === 'Sub-machine gun'
						?
						<Card key={product._id} className="productCards">
							<Card.Body>
								<Card.Img className="product-image" src={smg} />
								<Card.Title className="product-name">{product.name}</Card.Title>
								<Card.Subtitle className="product-category">{product.category}</Card.Subtitle>
								<Card.Text className="product-text">{product.description}</Card.Text>
								<Card.Text className="product-text">${product.price}</Card.Text>
								{
									product.sale.saleStatus
									?
									<>
										<Card.Text className="product-text onSale">On Sale</Card.Text>
										<Card.Text className="product-text onSale">{product.sale.saleValue * 100}%</Card.Text>
									</>
									:
									null
								}
							</Card.Body>
						</Card>	
						:
						<Card key={product._id} className="productCards">
							<Card.Body>
								<Card.Img className="product-image" src={rifle} />
								<Card.Title className="product-name">{product.name}</Card.Title>
								<Card.Subtitle className="product-category">{product.category}</Card.Subtitle>
								<Card.Text className="product-text">{product.description}</Card.Text>
								<Card.Text className="product-text">${product.price}</Card.Text>
								{
									product.sale.saleStatus
									?
									<>
										<Card.Text className="product-text onSale">On Sale</Card.Text>
										<Card.Text className="product-text onSale">{product.sale.saleValue * 100}%</Card.Text>
									</>
									:
									null
								}
							</Card.Body>
						</Card>	

				}
			</Col>
			:
			<Col xs={12} md={4} className="productColumns">
				{
					product.category === 'Handgun'
					?
					<Card key={product._id} className="productCards" onClick={() => orderProduct(product._id)}>
						<Card.Body>
							<Card.Img className="product-image" src={handgun} />
							<Card.Title className="product-name">{product.name}</Card.Title>
							<Card.Subtitle className="product-category">{product.category}</Card.Subtitle>
							<Card.Text className="product-text">{product.description}</Card.Text>
							<Card.Text className="product-text">${product.price}</Card.Text>
							{
								product.sale.saleStatus
								?
								<>
									<Card.Text className="product-text onSale">On Sale</Card.Text>
									<Card.Text className="product-text onSale">{product.sale.saleValue * 100}%</Card.Text>
								</>
								:
								null
							}
						</Card.Body>
					</Card>	
					:
						product.category === 'Sub-machine gun'
						?
						<Card key={product._id} className="productCards" onClick={() => orderProduct(product._id)}>
							<Card.Body>
								<Card.Img className="product-image" src={smg} />
								<Card.Title className="product-name">{product.name}</Card.Title>
								<Card.Subtitle className="product-category">{product.category}</Card.Subtitle>
								<Card.Text className="product-text">{product.description}</Card.Text>
								<Card.Text className="product-text">${product.price}</Card.Text>
								{
									product.sale.saleStatus
									?
									<>
										<Card.Text className="product-text onSale">On Sale</Card.Text>
										<Card.Text className="product-text onSale">{product.sale.saleValue * 100}%</Card.Text>
									</>
									:
									null
								}
							</Card.Body>
						</Card>	
						:
						<Card key={product._id} className="productCards" onClick={() => orderProduct(product._id)}>
							<Card.Body>
								<Card.Img className="product-image" src={rifle} />
								<Card.Title className="product-name">{product.name}</Card.Title>
								<Card.Subtitle className="product-category">{product.category}</Card.Subtitle>
								<Card.Text className="product-text">{product.description}</Card.Text>
								<Card.Text className="product-text">${product.price}</Card.Text>
								{
									product.sale.saleStatus
									?
									<>
										<Card.Text className="product-text onSale">On Sale</Card.Text>
										<Card.Text className="product-text onSale">{product.sale.saleValue * 100}%</Card.Text>
									</>
									:
									null
								}
							</Card.Body>
						</Card>	

				}
			</Col>
			
			)

	})

	return(
		willRedirect
		?
		<>
			<OrderProduct productProp={localStorage.getItem('productId')} />
			<Redirect to="/orderProduct" />

		</>
		:
		<>
			<Jumbotron id="productJumbotron" fluid>
				<div id="productBanner-content">
					<h1 id="productBanner-header">Products Section</h1>
					<p id="productBanner-paragraph">
					    Choose your products here.
					</p>
				</div>
			</Jumbotron>
			<Container>
				<Row>
					{productsCard}
				</Row>
			</Container>
		</>

		)

}