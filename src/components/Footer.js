import React, {useState} from 'react'

// Bootstrap Components
import {Container, Row, Col} from 'react-bootstrap'

// React Icons
import {FaReddit, FaFacebook, FaTwitterSquare} from 'react-icons/fa'

export default function Footer(){

	function logoToHome(){

		window.location.replace('/')

	}

	return(

		<div className="footer">
			<Container className="footer-content">
				<Row>
					<Col md={6}>
						<h2 id="footer-brand" onClick={logoToHome}>Ammunation</h2>
					</Col>
					<Col md={4}>
						<p className="contact-and-follow">Contact Info</p>
						<Row>
							<Col>
								<p>Mount Everest, Nepal.</p>
							</Col>
						</Row>
					</Col>
					<Col md={2}>
						<p className="contact-and-follow">Follow Us:</p>
						<Row>
							<Col sm={12} md={4}>
								<a href="https://www.facebook.com/" target="_blank"><FaFacebook className="footer-icons" /></a>
							</Col>
							<Col sm={12} md={4}>
								<a href="https://twitter.com/jasonababat" target="_blank"><FaTwitterSquare className="footer-icons" /></a>
							</Col>
							<Col sm={12} md={4}>
								<a href="https://www.reddit.com/" target="_blank"><FaReddit className="footer-icons" /></a>
							</Col>
						</Row>
					</Col>
				</Row>
			</Container>
			<p id="copyright">Copyright &#169; 2021. All Rights Reserved</p>
		</div>

	)

}