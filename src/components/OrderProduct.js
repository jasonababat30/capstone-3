import React, {useState, useEffect, useContext} from 'react'

// UserContext
import UserContext from '../userContext'

// Bootstrap
import {Container, Card, Form, Button, Row, Col} from 'react-bootstrap'

// Sweetalert2
import Swal from 'sweetalert2'

// Redirect
import {Redirect} from 'react-router-dom'

// Images
import handgun from '../images/handgun-category.png'
import smg from '../images/smg-category.png'
import rifle from '../images/rifle-category.png'
import logo from '../images/cowboy.png'

export default function OrderProduct({productProp}){
	
	//States
	const [product, setProduct] = useState({})
	const [orderQuantity, setOrderQuantity] = useState(0)
	const [isActive,setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)
	const {user,setUser} = useContext(UserContext)

	let productId = localStorage.getItem('productId')

	//UseEffect
	useEffect(() => {

		fetch(`https://floating-taiga-76973.herokuapp.com/amntn/products/${productId}`,{

			headers: {

				'Authorization' : `Bearer ${localStorage.getItem('token')}`

			}

		}).then(response => response.json()).then(data => {

			console.log(data)
			setProduct(data.data)

		})

	},[])

	useEffect(() => {

		if(orderQuantity !== 0){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	})

	function orderProduct(e){

		let newPrice = product.price - (product.price * product.sale.saleValue);
		console.log(newPrice)
		let priceNew = newPrice - (newPrice * localStorage.getItem('couponValue'));
		console.log(priceNew)
		let newCash = user.cash - priceNew
		console.log(newCash)


		e.preventDefault()

		fetch(`https://floating-taiga-76973.herokuapp.com/amntn/order`, {

			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				productName: product.name,
				orderQuantity: orderQuantity

			})

		}).then(response => response.json()).then(data => {

			if(!data.data){

				Swal.fire({
					icon: "error",
					title: "Product Purchase Unsuccessful!",
					text: data.message
				})

				setWillRedirect(true)

			} else {

				if(user.sex === 'Male'){

					Swal.fire({

						icon: "success",
						title: "Product Purchase Successful!",
						text: `Mr. ${user.fullName}, thank you for purchasing ${product.name}!`

					})

					localStorage.setItem('cash', newCash)
					setWillRedirect(true)

				} else {

					Swal.fire({

						icon: "success",
						title: "Product Purchase Successful!",
						text: `Mrs. ${user.fullName}, thank you for purchasing ${product.name}!`

					})

					localStorage.setItem('cash', newCash)
					setWillRedirect(true)

				}

			}

		})

		setProduct({})
		setOrderQuantity(0)

	}


	console.log(product)
	// console.log(productProp)

	return(
			willRedirect
			?
			<Redirect to="/products" />
			:
				!user.email || user.isAdmin
				?
				<Redirect to="/" />
				:
				<Container id="order-container">
					<Card id="order-card">
						<Card.Body>
						<Row>
							<Col xs={12} md={6}>
								<Card.Img src={
									product.category === 'Handgun'
									?
									handgun
									:
										product.category === 'Sub-machine gun'
										?
										smg
										:
											product.category === 'Rifle'
											?
											rifle
											:
											logo
								} alt="gun picture" id="order-image" className="img-fluid" />
							</Col>
							<Col xs={12} md={6}>
								
								<Card.Title id="order-title">{product.name}</Card.Title>
								<Card.Subtitle id="order-subtitle">{product.category}</Card.Subtitle>
								<Card.Text id="order-price">${product.price}</Card.Text>
								<Form id="order-form" onSubmit={e => orderProduct(e)}>
									<Form.Group>
										<Form.Label>Number of Orders</Form.Label>
										<Form.Control type="number" placeholder="Enter the Number of Orders" onChange={e => {setOrderQuantity(e.target.value)}} />
									</Form.Group>
									{
										isActive
										? <Button className="order-button" variant="danger" type="submit">Buy</Button>
										: <Button className="order-button" variant="danger" type="submit" disabled>Buy</Button>
									}
								</Form>	
							</Col>
						</Row>
						</Card.Body>
					</Card>
				</Container>

		)

}
