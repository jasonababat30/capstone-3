import React from 'react'

// Images
import handguns from '../images/handguns.jpg'
import bullets from '../images/bullets.jpg'
import rifle from '../images/rifle.jpg'
import revolver from '../images/revolver.jpg'
import ak from '../images/ak.jpg'

// Bootstrap Components
import {Container, Figure, Row, Col} from 'react-bootstrap'

export default function Highlights(){

	return(
		<>
			<h1 id="highlights-header">Top Sale Products</h1>
			<Row className="highlights-row">
				<Col xs={12} md={4}>
					<Figure>
						<Figure.Image src={handguns} alt="pistols" className="highlights-images img-fluid" />
						<Figure.Caption className="bottom-left">
							<h1>Pistols</h1>
						</Figure.Caption>
					</Figure>
				</Col>
				<Col xs={12} md={4}>
					<Figure>
						<Figure.Image src={bullets} alt="rifle ammunition" className="highlights-images img-fluid" />
						<Figure.Caption className="bottom-left">
							<h1>Rifle Ammunition</h1>
						</Figure.Caption>
					</Figure>
				</Col>
				<Col xs={12} md={4}>
					<Figure>
						<Figure.Image src={revolver} alt="revolver" className="highlights-images img-fluid" />
						<Figure.Caption className="bottom-left">
							<h1>Revolver</h1>
						</Figure.Caption>
					</Figure>
				</Col>
				<Col xs={12} md={6}>
					<Figure>
						<Figure.Image src={rifle} alt="special carbine" className="highlights-images img-fluid" />
						<Figure.Caption className="bottom-left">
							<h1>Special Carbine</h1>
						</Figure.Caption>
					</Figure>
				</Col>
				<Col xs={12} md={6}>
					<Figure>
						<Figure.Image src={ak} alt="Assault Rifle" className="highlights-images img-fluid" />
						<Figure.Caption className="bottom-left">
							<h1>Assault Rifle</h1>
						</Figure.Caption>
					</Figure>
				</Col>
			</Row>
		</>
		

		)

}