import React, {useState, useEffect, useContext} from 'react'

// Bootstrap
import {Table, Button, Container} from 'react-bootstrap'

// User Context
import UserContext from '../userContext'

export default function AdminDashboard(){

	// State
	const [allProducts, setAllProducts] = useState([])
	const [update,setUpdate] = useState(0)

	useEffect(() => {

		fetch('https://floating-taiga-76973.herokuapp.com/amntn/products').then(response => response.json()).then(data => {

			setAllProducts(data.data)

		})

	},[update])

	// console.log(allProducts)

	function archive(productId){

		fetch(`https://floating-taiga-76973.herokuapp.com/amntn/archives/${productId}`, {

			method: "PUT",
			headers: {

				'Authorization' : `Bearer ${localStorage.getItem('token')}`

			}

		}).then(response => response.json()).then(data => {

			setUpdate({})

		})

	}

	function activate(productId){

		fetch(`https://floating-taiga-76973.herokuapp.com/amntn/archives/turnToActive/${productId}`, {

			method: "PUT",
			headers: {

				'Authorization' : `Bearer ${localStorage.getItem('token')}`

			}

		}).then(response => response.json()).then(data => {

			setUpdate({})

		})

	}

	// console.log(allProducts)

	let productsAll = allProducts.map(product => {

		return(

				<tr>
					<td>{product._id}</td>
					<td id="product-name">{product.name}</td>
					<td id="product-status" className={product.isActive ? "text-success" : "text-danger"}>
						{
							product.isActive
							?
							"Active"
							:
							"Inactive"
						}
					</td>
					<td>
						{
							product.isActive

							?

							<Button variant="danger" className="mx-2" onClick={() => archive(product._id)}>Archive</Button>

							:

							<Button variant="success" className="mx-2" onClick={() => activate(product._id)}>Activate</Button>

						}
					</td>
				</tr>

			)

	})

	return(

		<>
		<Container id="dashboard-container" className="container-fluid">
			<h1 id="dashboard-heading">Admin Dashboard</h1>
			<Table id="dashboard-table" responsive>
				<thead id="dashboardTable-heading">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productsAll}
				</tbody>
			</Table>
		</Container>
		</>

		)

}